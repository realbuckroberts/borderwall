#!/usr/bin/pyton
import os
import argparse
import subprocess
#http://realbuckroberts.com or find me on your favorite social media and music streaming sites.

parser = argparse.ArgumentParser()
parser.add_argument("--list", "-l", help="File containing list of IP ranges")
parser.add_argument("--output", "-o", help="output file for expanded list")
args = parser.parse_args()
list=args.list
output=args.output
pwd=os.getcwd()


def makerange(begining, ending, output):
    b1, b2, b3, b4 = begining.split('.')
    e1, e2, e3, e4 = ending.split('.')
    with open(output, 'a') as outfile:
        for a in range(int(b1),int(e1)+1):
            for b in range(int(b2),int(e2)+1):
                for c in range(int(b3),int(e3)+1):
                    for d in range(int(b4),int(e4)+1):
                        ipinrange=str(a)+'.'+str(b)+'.'+str(c)+'.'+str(d)
                        outfile.write(ipinrange+'\n')
                        print ipinrange
    outfile.close()
    grantoutput='chmod 777 '+output
    os.system(grantoutput)


def readips(inputfile, output):
    grantoutput='chmod 777 '+inputfile
    os.system(inputfile)
    with open(inputfile, 'r') as infile:
        for line in infile:
            theip=line.strip()
            #print theip
            beginip, endip = theip.split("-")
            #print beginip, endip
            makerange(beginip, endip, output)
    infile.close()

readips(list, output)