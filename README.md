I was looking for ways to block ranges of IPs in case of large scale cyber attacks.
I found this site that will give you IP address ranges by country or region and it will even help you write firewall rules.
https://www.countryipblocks.net/acl.php They look something like this x.x.x.x-x.x.x.x.
But in case you need an expanded list of IPs from that range here is a python script for generating that list.

Usage:

Optional arguments:
  -h, --help            show this help message and exit
  --list LIST, -l LIST  File containing list of IP ranges
  --output OUTPUT, -o OUTPUT
                        output file for expanded list

Example: python borderwall.py --list ipranges.txt --output expanded.txt